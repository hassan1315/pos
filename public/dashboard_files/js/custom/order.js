$(document).ready(function() {
    //add product btn
    $(".add-product-btn").on("click", function(e) {
        e.preventDefault();
        var name = $(this).data("name");
        var id = $(this).data("id");
        var price = $.number($(this).data("price"), 2);

        $(this)
            .removeClass("btn-success")
            .addClass("btn-default disabled");

        var html = `<tr>
                <td>${name}</td>
                <td><input type="number" name="products[${id}][quantity]" data-price="${price}" class="form-control input-sm product-quantity" min="1" value="1"></td>
                <td class="product-price">${price}</td>               
                <td><button class="btn btn-danger btn-sm remove-product-btn" data-id="${id}"><span class="fa fa-trash"></span></button></td>
            </tr>`;

        $(".order-list").append(html);

        //to calculate total price
        calculateTotal();
    });

    //disabled btn
    $("body").on("click", ".disabled", function(e) {
        e.preventDefault();
    }); //end of disabled

    //remove product btn
    $("body").on("click", ".remove-product-btn", function(e) {
        e.preventDefault();
        var id = $(this).data("id");

        $(this)
            .closest("tr")
            .remove();
        $("#product-" + id)
            .removeClass("btn-default disabled")
            .addClass("btn-success");

        //to calculate total price
        calculateTotal();
    }); //end of remove product btn

    //change product quantity
    $("body").on("keyup change", ".product-quantity", function() {
        var quantity = Number($(this).val());
        var unitPrice = parseFloat(
            $(this)
                .data("price")
                .replace(/,/g, "")
        );
        $(this)
            .closest("tr")
            .find(".product-price")
            .html($.number(quantity * unitPrice, 2));
        calculateTotal();
    }); //end of product quantity change

    //list all order products
    $(".order-products").on("click", function(e) {
        e.preventDefault();

        var url = $(this).data("url");
        var method = $(this).data("method");
        $("#loading").css("display", "flex");
        $.ajax({
            url: url,
            method: method,
            success: function(data) {
                $("#order-product-list").empty();
                $("#order-product-list").append(data);
                $("#loading").css("display", "none");
            }
        });
    }); //end of order products click

    //list all order products
    $(".order-status-btn").on("click", function(e) {
        e.preventDefault();

        var url = $(this).data("url");
        var method = $(this).data("method");
        var id = $(this).data("id");
        $.ajax({
            url: url,
            method: method,
            data: { status: status },
            success: function(data) {
                $("#status_data" + id + "").empty();
                $("#status_data" + id + "").append(data);
            }
        });
    }); //end of order products click

    //print order
    $(document).on("click", ".print-btn", function() {
        $("#print-area").printThis();
    }); //end of click function

    $(".search-product").on("click", function(e) {
        e.preventDefault();
        var url = $(this).data("url");
        var method = $(this).data("method");
        var category = $(this).data("category");
        var search = $(this)
            .parent()
            .siblings(".search-pro")
            .val();
        $.ajax({
            url: url,
            method: method,
            data: { search: search, category: category },
            success: function(data) {
                $("#product-filter-" + category + "").empty();
                $("#product-filter-" + category + "").append(data);
            }
        });
    }); //end of search product in order

    $(".btn-report").on("click", function(e) {
        e.preventDefault();

        $("#loading").css("display", "flex");
        var url = $(this).data("url");
        var method = $(this).data("method");
        var year = $(".year").val();
        var month = $(".month").val();
        var day = $(".day").val();
        $.ajax({
            url: url,
            method: method,
            data: { year: year, month: month, day: day },
            success: function(data) {
                $("#loading").css("display", "none");
                $("#report").empty();
                $("#report").append(data);
            }
        });
    }); //end of get report dailry

    $(".btn-monthyReport    ").on("click", function(e) {
        e.preventDefault();
        $("#loading").css("display", "flex");
        var url = $(this).data("url");
        var method = $(this).data("method");
        var begain_day = $(".begain_day").val();
        var last_day = $(".last_day").val();
        var month = $(".month").val();
        var year = $(".year").val();
        $.ajax({
            url: url,
            method: method,
            data: {
                begain_day: begain_day,
                last_day: last_day,
                month: month,
                year: year
            },

            success: function(data) {
                $("#loading").css("display", "none");
                $("#monthlyReport").empty();
                $("#monthlyReport").append(data);
            }
        });
    }); //end of get report monthly

    $(".btn-yearlyReport").on("click", function(e) {
        e.preventDefault();
        $("#loading").css("display", "flex");
        var url = $(this).data("url");
        var method = $(this).data("method");
        var begain_month = $(".begain_month").val();
        var last_month = $(".last_month").val();
        var year = $(".year").val();
        $.ajax({
            url: url,
            method: method,
            data: {
                begain_month: begain_month,
                last_month: last_month,
                year: year
            },

            success: function(data) {
                $("#loading").css("display", "none");
                $("#yearlyReport").empty();
                $("#yearlyReport").append(data);
            }
        });
    }); //end of get report monthly
}); //end of document ready

//calculate the total
function calculateTotal() {
    var price = 0;
    $(".order-list .product-price").each(function(index) {
        price += parseFloat(
            $(this)
                .html()
                .replace(/,/g, "")
        );
    }); //end of product price

    $(".total-price").html($.number(price, 2));
    $(".total_price").val($.number(price, 2));

    //check if price > 0
    if (price > 0) {
        $("#add-order-form-btn").removeClass("disabled");
    } else {
        $("#add-order-form-btn").addClass("disabled");
    } //end of else
} //end of calculate total
