-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 24, 2019 at 02:59 AM
-- Server version: 10.3.15-MariaDB
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pos`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `created_at`, `updated_at`) VALUES
(1, '2019-09-23 12:28:20', '2019-09-23 12:28:20'),
(2, '2019-09-23 12:28:20', '2019-09-23 12:28:20'),
(3, '2019-09-23 12:28:21', '2019-09-23 12:28:21'),
(4, '2019-09-23 12:28:22', '2019-09-23 12:28:22');

-- --------------------------------------------------------

--
-- Table structure for table `category_translations`
--

CREATE TABLE `category_translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `category_translations`
--

INSERT INTO `category_translations` (`id`, `category_id`, `name`, `locale`) VALUES
(1, 1, 'قسم الالكترونيات', 'ar'),
(2, 1, 'Tv Category', 'en'),
(3, 2, 'قسم الكمبيوتر ', 'ar'),
(4, 2, 'Computer Category', 'en'),
(5, 3, 'قسم الاجهزه الكهربية  ', 'ar'),
(6, 3, 'Clothes Wash Category', 'en'),
(7, 4, 'قسم الملابس', 'ar'),
(8, 4, 'Clothes Category', 'en');

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

CREATE TABLE `clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `clients`
--

INSERT INTO `clients` (`id`, `name`, `phone`, `address`, `created_at`, `updated_at`) VALUES
(1, 'حسن محمد', '\"01005164154\"', 'قليوب البلد حى سيدى عبد الرحمن ', '2019-09-23 12:28:28', '2019-09-23 12:28:28'),
(2, 'ايهاب السيسى', '\"01078965896\"', 'الخانكة طريق شبين القناطر/ سندوه', '2019-09-23 12:28:28', '2019-09-23 12:28:28'),
(3, 'حميد بلابل', '\"01064564868\"', 'المنوفيه / قويسنا طه شبرا', '2019-09-23 12:28:28', '2019-09-23 12:28:28'),
(4, 'اشرف داود', '\"01138464882\"', 'القناطر الخيريه طريق بلتان', '2019-09-23 12:28:28', '2019-09-23 12:28:28'),
(5, 'احمد البرادعى', '\"01568464532\"', 'الغربية طنطا كفر مسعود', '2019-09-23 12:28:28', '2019-09-23 12:28:28'),
(6, 'مستخدم تجريبى', '[\"01005164154\"]', 'عنوان تجريبى', '2019-09-23 19:30:19', '2019-09-23 19:30:19');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_01_10_124214_laratrust_setup_tables', 1),
(4, '2019_01_24_145852_create_categories_table', 1),
(5, '2019_01_28_194130_create_category_translations_table', 1),
(6, '2019_02_05_031022_create_products_table', 1),
(7, '2019_02_05_031035_create_product_translations_table', 1),
(8, '2019_02_19_064134_create_clients_table', 1),
(9, '2019_02_27_224902_create_orders_table', 1),
(10, '2019_06_06_105828_create_product_order_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `total_price` double(8,2) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `client_id`, `total_price`, `status`, `created_at`, `updated_at`) VALUES
(16, 1, 14760.00, 1, '2019-02-28 22:00:00', '2019-01-01 03:31:13'),
(18, 3, 110.00, 1, '2019-01-31 22:00:00', '2019-09-23 13:37:22'),
(19, 1, 16760.00, 1, '2019-03-23 13:32:01', '2019-09-23 13:37:23'),
(20, 5, 9450.00, 1, '2019-05-23 13:32:29', '2019-09-23 13:37:25'),
(21, 1, 7650.00, 1, '2019-05-23 13:32:42', '2019-09-23 13:37:26'),
(22, 5, 16760.00, 1, '2019-05-23 13:32:54', '2019-09-23 13:37:28'),
(23, 2, 15270.00, 1, '2019-06-23 13:33:09', '2019-09-23 13:38:51'),
(25, 2, 19250.00, 1, '2019-08-22 22:00:00', '2019-09-23 16:19:42'),
(26, 3, 9450.00, 1, '2019-06-23 13:33:58', '2019-09-23 13:36:59'),
(27, 2, 7650.00, 1, '2019-07-23 13:34:27', '2019-09-23 13:37:03'),
(28, 4, 10850.00, 1, '2019-07-23 13:34:37', '2019-09-23 13:37:05'),
(29, 5, 6250.00, 1, '2019-09-22 22:00:00', '2019-09-23 13:37:07'),
(30, 4, 15270.00, 1, '2019-08-23 13:34:57', '2019-09-23 13:37:09'),
(40, 5, 1210.00, 1, '2019-08-23 13:50:07', '2019-09-23 13:51:23'),
(41, 1, 9450.00, 1, '2019-07-23 13:51:22', '2019-09-23 13:51:29'),
(49, 4, 20650.00, 1, '2019-08-02 22:00:00', '2019-09-23 16:56:25'),
(50, 2, 7650.00, 1, '2019-09-20 16:56:45', '2019-09-23 16:57:05'),
(51, 4, 10750.00, 1, '2019-09-19 16:56:49', '2019-09-23 16:57:03'),
(52, 3, 3760.00, 1, '2019-08-23 16:56:52', '2019-09-23 16:57:01'),
(53, 3, 4950.00, 1, '2019-07-23 16:56:56', '2019-09-23 16:56:58'),
(54, 1, 5350.00, 1, '2019-08-23 16:58:20', '2019-09-23 16:58:23'),
(55, 2, 660.00, 1, '2019-09-20 17:39:36', '2019-09-23 17:39:46'),
(56, 2, 8850.00, 1, '2019-09-23 18:13:47', '2019-09-23 18:14:08'),
(62, 3, 660.00, 1, '2019-09-23 18:29:40', '2019-09-23 19:30:50'),
(63, 2, 15880.00, 1, '2019-09-23 18:29:51', '2019-09-23 19:30:51'),
(64, 5, 6500.00, 1, '2019-09-23 18:30:37', '2019-09-23 19:30:52'),
(65, 3, 660.00, 1, '2019-09-23 18:30:46', '2019-09-23 19:30:53'),
(66, 4, 7550.00, 1, '2019-09-23 18:31:05', '2019-09-23 19:30:54'),
(67, 1, 2750.00, 1, '2019-09-23 18:31:09', '2019-09-23 19:30:54'),
(69, 6, 4460.00, 1, '2019-09-23 19:30:42', '2019-09-23 19:30:48'),
(70, 1, 11650.00, 1, '2019-09-23 19:31:35', '2019-09-23 19:31:40');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `display_name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'create_categories', 'Create Categories', 'Create Categories', '2019-09-23 12:28:17', '2019-09-23 12:28:17'),
(2, 'read_categories', 'Read Categories', 'Read Categories', '2019-09-23 12:28:17', '2019-09-23 12:28:17'),
(3, 'update_categories', 'Update Categories', 'Update Categories', '2019-09-23 12:28:17', '2019-09-23 12:28:17'),
(4, 'delete_categories', 'Delete Categories', 'Delete Categories', '2019-09-23 12:28:17', '2019-09-23 12:28:17'),
(5, 'create_products', 'Create Products', 'Create Products', '2019-09-23 12:28:17', '2019-09-23 12:28:17'),
(6, 'read_products', 'Read Products', 'Read Products', '2019-09-23 12:28:17', '2019-09-23 12:28:17'),
(7, 'update_products', 'Update Products', 'Update Products', '2019-09-23 12:28:17', '2019-09-23 12:28:17'),
(8, 'delete_products', 'Delete Products', 'Delete Products', '2019-09-23 12:28:17', '2019-09-23 12:28:17'),
(9, 'create_clients', 'Create Clients', 'Create Clients', '2019-09-23 12:28:17', '2019-09-23 12:28:17'),
(10, 'read_clients', 'Read Clients', 'Read Clients', '2019-09-23 12:28:17', '2019-09-23 12:28:17'),
(11, 'update_clients', 'Update Clients', 'Update Clients', '2019-09-23 12:28:18', '2019-09-23 12:28:18'),
(12, 'delete_clients', 'Delete Clients', 'Delete Clients', '2019-09-23 12:28:18', '2019-09-23 12:28:18'),
(13, 'create_orders', 'Create Orders', 'Create Orders', '2019-09-23 12:28:18', '2019-09-23 12:28:18'),
(14, 'read_orders', 'Read Orders', 'Read Orders', '2019-09-23 12:28:18', '2019-09-23 12:28:18'),
(15, 'update_orders', 'Update Orders', 'Update Orders', '2019-09-23 12:28:18', '2019-09-23 12:28:18'),
(16, 'delete_orders', 'Delete Orders', 'Delete Orders', '2019-09-23 12:28:18', '2019-09-23 12:28:18'),
(17, 'create_users', 'Create Users', 'Create Users', '2019-09-23 12:28:18', '2019-09-23 12:28:18'),
(18, 'read_users', 'Read Users', 'Read Users', '2019-09-23 12:28:18', '2019-09-23 12:28:18'),
(19, 'update_users', 'Update Users', 'Update Users', '2019-09-23 12:28:18', '2019-09-23 12:28:18'),
(20, 'delete_users', 'Delete Users', 'Delete Users', '2019-09-23 12:28:18', '2019-09-23 12:28:18');

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(17, 1),
(18, 1),
(19, 1),
(20, 1);

-- --------------------------------------------------------

--
-- Table structure for table `permission_user`
--

CREATE TABLE `permission_user` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `user_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_user`
--

INSERT INTO `permission_user` (`permission_id`, `user_id`, `user_type`) VALUES
(1, 1, 'App\\User'),
(2, 1, 'App\\User'),
(3, 1, 'App\\User'),
(4, 1, 'App\\User'),
(5, 1, 'App\\User'),
(6, 1, 'App\\User'),
(7, 1, 'App\\User'),
(8, 1, 'App\\User'),
(9, 1, 'App\\User'),
(10, 1, 'App\\User'),
(11, 1, 'App\\User'),
(12, 1, 'App\\User'),
(13, 1, 'App\\User'),
(14, 1, 'App\\User'),
(15, 1, 'App\\User'),
(16, 1, 'App\\User'),
(17, 1, 'App\\User'),
(18, 1, 'App\\User'),
(19, 1, 'App\\User'),
(20, 1, 'App\\User'),
(1, 2, 'App\\User'),
(2, 2, 'App\\User'),
(3, 2, 'App\\User'),
(4, 2, 'App\\User'),
(5, 2, 'App\\User'),
(6, 2, 'App\\User'),
(7, 2, 'App\\User'),
(8, 2, 'App\\User'),
(9, 2, 'App\\User'),
(10, 2, 'App\\User'),
(11, 2, 'App\\User'),
(12, 2, 'App\\User'),
(13, 2, 'App\\User'),
(14, 2, 'App\\User'),
(15, 2, 'App\\User'),
(16, 2, 'App\\User'),
(17, 2, 'App\\User'),
(18, 2, 'App\\User'),
(19, 2, 'App\\User'),
(20, 2, 'App\\User'),
(2, 3, 'App\\User'),
(3, 3, 'App\\User'),
(4, 3, 'App\\User'),
(6, 3, 'App\\User'),
(7, 3, 'App\\User'),
(8, 3, 'App\\User'),
(10, 3, 'App\\User'),
(18, 3, 'App\\User'),
(19, 3, 'App\\User'),
(20, 3, 'App\\User'),
(2, 4, 'App\\User'),
(3, 4, 'App\\User'),
(6, 4, 'App\\User'),
(7, 4, 'App\\User'),
(10, 4, 'App\\User'),
(11, 4, 'App\\User'),
(14, 4, 'App\\User'),
(15, 4, 'App\\User'),
(18, 4, 'App\\User'),
(19, 4, 'App\\User'),
(2, 6, 'App\\User'),
(6, 6, 'App\\User'),
(10, 6, 'App\\User'),
(14, 6, 'App\\User'),
(18, 6, 'App\\User');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'default.png',
  `purchase_price` double(8,2) NOT NULL,
  `sale_price` double(8,2) NOT NULL,
  `stock` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `category_id`, `image`, `purchase_price`, `sale_price`, `stock`, `created_at`, `updated_at`) VALUES
(1, 1, 'default.png', 1500.00, 2000.00, 70, '2019-09-23 12:28:22', '2019-09-23 19:31:35'),
(2, 1, 'default.png', 2000.00, 2300.00, 93, '2019-09-23 12:28:22', '2019-09-23 19:31:35'),
(3, 1, 'default.png', 1500.00, 2200.00, 92, '2019-09-23 12:28:23', '2019-09-23 19:31:35'),
(4, 1, 'default.png', 380.00, 450.00, 90, '2019-09-23 12:28:24', '2019-09-23 19:31:35'),
(5, 1, 'default.png', 600.00, 700.00, 91, '2019-09-23 12:28:24', '2019-09-23 19:31:35'),
(6, 2, 'default.png', 12000.00, 13000.00, 7, '2019-09-23 12:28:24', '2019-09-23 18:29:51'),
(7, 2, 'default.png', 1500.00, 2000.00, 35, '2019-09-23 12:28:24', '2019-09-23 19:30:42'),
(8, 2, 'default.png', 650.00, 700.00, 91, '2019-09-23 12:28:24', '2019-09-23 19:30:42'),
(9, 2, 'default.png', 120.00, 180.00, 32, '2019-09-23 12:28:24', '2019-09-23 19:30:42'),
(10, 2, 'default.png', 120.00, 180.00, 33, '2019-09-23 12:28:25', '2019-09-23 19:30:42'),
(11, 2, 'default.png', 500.00, 700.00, 35, '2019-09-23 12:28:25', '2019-09-23 19:30:42'),
(12, 3, 'default.png', 3000.00, 3200.00, 13, '2019-09-23 12:28:25', '2019-09-23 18:31:05'),
(13, 3, 'default.png', 2300.00, 3500.00, 31, '2019-09-23 12:28:26', '2019-09-23 18:31:05'),
(14, 3, 'default.png', 800.00, 850.00, 21, '2019-09-23 12:28:26', '2019-09-23 18:31:05'),
(15, 3, 'default.png', 1200.00, 1300.00, 22, '2019-09-23 12:28:26', '2019-09-23 18:13:47'),
(16, 3, 'default.png', 500.00, 600.00, 23, '2019-09-23 12:28:26', '2019-09-23 16:56:56'),
(17, 4, 'default.png', 180.00, 230.00, 93, '2019-09-23 12:28:26', '2019-09-23 18:30:46'),
(18, 4, 'default.png', 200.00, 230.00, 93, '2019-09-23 12:28:27', '2019-09-23 18:30:46'),
(19, 4, 'default.png', 170.00, 200.00, 93, '2019-09-23 12:28:27', '2019-09-23 18:30:46'),
(20, 4, 'default.png', 300.00, 350.00, 56, '2019-09-23 12:28:27', '2019-09-23 13:50:07');

-- --------------------------------------------------------

--
-- Table structure for table `product_order`
--

CREATE TABLE `product_order` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `order_id` int(10) UNSIGNED NOT NULL,
  `quantity` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_order`
--

INSERT INTO `product_order` (`id`, `product_id`, `order_id`, `quantity`) VALUES
(76, 6, 16, 1),
(77, 8, 16, 1),
(78, 9, 16, 1),
(79, 10, 16, 1),
(80, 11, 16, 1),
(85, 17, 18, 1),
(86, 18, 18, 1),
(87, 19, 18, 1),
(88, 20, 18, 1),
(90, 7, 19, 1),
(91, 6, 19, 1),
(92, 8, 19, 1),
(93, 9, 19, 1),
(94, 10, 19, 1),
(95, 11, 19, 1),
(96, 12, 20, 1),
(97, 13, 20, 1),
(98, 14, 20, 1),
(99, 15, 20, 1),
(100, 16, 20, 1),
(101, 5, 21, 1),
(102, 4, 21, 1),
(103, 3, 21, 1),
(104, 2, 21, 1),
(105, 1, 21, 1),
(106, 11, 22, 1),
(107, 10, 22, 1),
(108, 9, 22, 1),
(109, 8, 22, 1),
(110, 7, 22, 1),
(111, 6, 22, 1),
(113, 20, 23, 1),
(114, 19, 23, 1),
(115, 18, 23, 1),
(116, 17, 23, 1),
(117, 6, 23, 1),
(118, 9, 23, 1),
(119, 10, 23, 1),
(120, 8, 23, 1),
(126, 16, 25, 1),
(127, 15, 25, 1),
(128, 14, 25, 1),
(129, 13, 25, 1),
(130, 6, 25, 1),
(131, 16, 26, 1),
(132, 15, 26, 1),
(133, 14, 26, 1),
(134, 12, 26, 1),
(135, 13, 26, 1),
(136, 1, 27, 1),
(137, 2, 27, 1),
(138, 3, 27, 1),
(139, 5, 27, 1),
(140, 4, 27, 1),
(141, 5, 28, 1),
(142, 4, 28, 1),
(143, 3, 28, 1),
(144, 2, 28, 1),
(145, 1, 28, 1),
(146, 12, 28, 1),
(147, 16, 29, 1),
(148, 15, 29, 1),
(149, 14, 29, 1),
(150, 13, 29, 1),
(152, 20, 30, 1),
(153, 19, 30, 1),
(154, 18, 30, 1),
(155, 17, 30, 1),
(156, 9, 30, 1),
(157, 10, 30, 1),
(158, 8, 30, 1),
(159, 6, 30, 1),
(205, 20, 40, 1),
(206, 19, 40, 1),
(207, 18, 40, 1),
(208, 17, 40, 1),
(209, 12, 41, 1),
(210, 13, 41, 1),
(211, 14, 41, 1),
(212, 15, 41, 1),
(213, 16, 41, 1),
(249, 5, 49, 1),
(250, 4, 49, 1),
(251, 3, 49, 1),
(252, 2, 49, 1),
(253, 1, 49, 1),
(254, 6, 49, 1),
(255, 1, 50, 1),
(256, 2, 50, 1),
(257, 3, 50, 1),
(258, 5, 50, 1),
(259, 4, 50, 1),
(260, 16, 51, 1),
(261, 14, 51, 1),
(262, 13, 51, 1),
(263, 12, 51, 1),
(264, 15, 51, 2),
(265, 11, 52, 1),
(266, 10, 52, 1),
(267, 9, 52, 1),
(268, 8, 52, 1),
(269, 7, 52, 1),
(270, 16, 53, 1),
(271, 14, 53, 1),
(272, 13, 53, 1),
(273, 1, 54, 1),
(274, 4, 54, 1),
(275, 5, 54, 1),
(276, 3, 54, 1),
(277, 17, 55, 1),
(278, 18, 55, 1),
(279, 19, 55, 1),
(280, 13, 56, 1),
(281, 14, 56, 1),
(282, 15, 56, 1),
(283, 12, 56, 1),
(304, 17, 62, 1),
(305, 18, 62, 1),
(306, 19, 62, 1),
(307, 9, 63, 1),
(308, 8, 63, 1),
(309, 6, 63, 1),
(310, 7, 63, 1),
(311, 1, 64, 1),
(312, 2, 64, 1),
(313, 3, 64, 1),
(314, 17, 65, 1),
(315, 18, 65, 1),
(316, 19, 65, 1),
(317, 12, 66, 1),
(318, 13, 66, 1),
(319, 14, 66, 1),
(320, 5, 67, 2),
(321, 4, 67, 3),
(327, 7, 69, 1),
(328, 8, 69, 2),
(329, 9, 69, 1),
(330, 10, 69, 1),
(331, 11, 69, 1),
(332, 1, 70, 3),
(333, 2, 70, 1),
(334, 3, 70, 1),
(335, 4, 70, 1),
(336, 5, 70, 1);

-- --------------------------------------------------------

--
-- Table structure for table `product_translations`
--

CREATE TABLE `product_translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_translations`
--

INSERT INTO `product_translations` (`id`, `product_id`, `name`, `description`, `locale`) VALUES
(1, 1, 'جهاز توشييا 32 بوصه', 'هذا وصف جهاز توشييا 32 بوصه', 'ar'),
(2, 1, 'Toshiba Tv 32 inches', 'this is description for ', 'en'),
(3, 2, 'تلفزيون سامسونج 48 بوصة', ' هذا وصف   ', 'ar'),
(4, 2, 'Tv samsong 32 inches', 'this is description for Tv samsong 32 inches', 'en'),
(5, 3, 'سماعة بلوتوث اير بود', 'هذا وصف سماعة بلوتوث اير بود', 'ar'),
(6, 3, 'Air Pods High Quality', 'this is description for air pods high quality', 'en'),
(7, 4, 'باور بانك 1000 ملى امبير', 'هذا وصف باور بانك 1000 ملى امبير', 'ar'),
(8, 4, 'Power Pank 10000 MA', 'this is description for Power Pank 10000 MA', 'en'),
(9, 5, 'سماعات عالية المستوى', 'هذ وثف سماعات عالية المستوى', 'ar'),
(10, 5, 'Sound Dj HIgh Sound', 'this is description for Sound Dj HIgh Sound ', 'en'),
(11, 6, 'لاب توب hp core i8 ram 8Gb', 'لاب توب hp core i8 هذا وصف', 'ar'),
(12, 6, 'laptop core i8 8gb ram', 'this is description for laptop core i8 8gb ram', 'en'),
(13, 7, 'شاشة كمبيوتر 32 بوصه ', 'شاشة كمبيوتر 32 بوصه ', 'ar'),
(14, 7, 'Lcd Computer 32 inches', 'this is description for Lcd Computer 32 inches', 'en'),
(15, 8, 'سماعات كمبيوتر عالية الدقة', 'هذا وصف سماعات كمبيوتر عالية الدقة', 'ar'),
(16, 8, 'Speaker HD High Quality', 'this is description for Speaker HD High Quality', 'en'),
(17, 9, 'كيبورد جيمينج', ' هذا وصف كيبورد جيمينج', 'ar'),
(18, 9, 'Keyboard Gaming', 'this is description for Keyboard Gaming', 'en'),
(19, 10, 'ماوس جيمينج', '<p>هذا وصف ماوس جيمينج</p>', 'ar'),
(20, 10, 'Mouse Gaming', 'this is description for Mouse Gaming', 'en'),
(21, 11, 'رامات كمبيوتر 8GB', ' هذا وصف رامات كمبيوتر 8GB ', 'ar'),
(22, 11, 'Ram 8GB Gaming', 'this is description for Ram 8GB Gaming', 'en'),
(23, 12, 'غسالة اوتماتيك توشيبا', 'هذا وصف غسالة اوتماتيك توشيبا', 'ar'),
(24, 12, 'Clothes Wash Autmatic', 'this is description for Clothes Wash Autmatic', 'en'),
(25, 13, 'تلاجة توشيبا 21 قدم', ' هذا وصف تلاجة توشيبا 21 قدم', 'ar'),
(26, 13, 'Frizze Toshiba 21 ', 'this is description for Frizze Toshiba 21', 'en'),
(27, 14, 'مروحة فريش اوتامتيك بريموت', 'هذا وصف مروجة فريش اوتامتيك بريموت', 'ar'),
(28, 14, 'Fan Fresh High Quality', 'this is description for Fan Fresh High Quality', 'en'),
(29, 15, 'غسالة ابطاق 25 بطق', '<p>هذا وصف غسالة ابطاق 25 بطق</p>', 'ar'),
(30, 15, 'dish Wash 23 dish', 'this is description for dish Wash 23 dish', 'en'),
(31, 16, 'مايكروف باشعه ليزر', 'هذا وصف مايكروف باشعه ليزر', 'ar'),
(32, 16, 'Microwave With Liser', 'this is description for Microwave With Liser', 'en'),
(33, 17, 'تي شيرت بولو سادة بطبعة لوجو ', 'هذ وصف تي شيرت بولو سادة بطبعة لوجو ', 'ar'),
(34, 17, 'T-shirt Solid with logo', 'this is description for T-shirt Solid with logo', 'en'),
(35, 18, 'سويت شيرت هودي بدون أكمام', 'هذ وصف سويت شيرت هودي بدون أكمام', 'ar'),
(36, 18, 'Swet T-shirt black ', 'this is description for Swet T-shirt', 'en'),
(37, 19, 'بنطلون بيجاما بنمط مربعات', '<p>هذ وصف بنطلون بيجاما بنمط مربعات</p>', 'ar'),
(38, 19, 'pantloan pjama blues', '<p>this is description for pantloan pjama blue</p>', 'en'),
(39, 20, 'جاكيت بومبر مزين بطبع', 'هذا وصف جاكيت بومبر مزين بطبع', 'ar'),
(40, 20, 'Jaket Bump With Logo', 'this is description for Jaket Bump With Logo', 'en');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'super_admin', 'Super Admin', 'Super Admin', '2019-09-23 12:28:17', '2019-09-23 12:28:17'),
(2, 'admin', 'Admin', 'Admin', '2019-09-23 12:28:20', '2019-09-23 12:28:20');

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE `role_user` (
  `role_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `user_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`role_id`, `user_id`, `user_type`) VALUES
(1, 1, 'App\\User'),
(2, 2, 'App\\User'),
(2, 3, 'App\\User'),
(2, 4, 'App\\User'),
(2, 6, 'App\\User');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'default.png',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `image`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'حسن', 'الهوارى', 'super_admin@app.com', 'BCfGNgPmcOpNcxnN9Nlt3OiVLymCaxMsklahYuA3.jpeg', NULL, '$2y$10$KwqFzpN/vWbf/jOJHzzcMudk7fOdp93iCXzQyh/LZbpalKe3K6ESe', 'wHr2wvVCQfyW3NO3yMUebVyhYug8XzIH1Ktoqbq53U8DDF7t4S8ZdaFg6q2e', '2019-09-23 12:28:20', '2019-09-23 17:39:06'),
(2, 'حسن', 'محمد', 'edit_delete_create@app.com', 'C4QKEB0pIqomYgGtt7nVH6He1GMEpJsN5BZFP2h0.jpeg', NULL, '$2y$10$k/k69//1MB6ZZgbbOXhsYOUv69E.fvDH30QrZCkCpf50.5H1E53KS', 'gcy5Sd03103THmWsvriH1frUXME5VEZPFxBlAbJQ0RSeWVM5bCQCxp4cxvU5', '2019-09-23 17:02:40', '2019-09-23 17:02:40'),
(3, 'ايهاب', 'السيسى', 'edit_delete@app.com', 'default.png', NULL, '$2y$10$PldhFWMisdkx.czjJYSUuOBJZlYjFT3P1pryo/2ZVuHVrKGS82zrC', 'oocrmhM8W1fvAY4WVlmQe8dqFWqsXaAhYzvhSpISfUHzCTwNaKZ243JZ7df0', '2019-09-23 17:03:35', '2019-09-23 17:03:35'),
(4, 'حميد', 'بلابل', 'edit@app.com', 'XHwCeW1hI8NONdAGK7Fz2YYR3dNW3gIpfrJg3i5q.jpeg', NULL, '$2y$10$TF0aJZ/QSdEpr97Kv7mZjeNFC4MYZP3.p/Z7lBi5K3J0VPtkguHie', NULL, '2019-09-23 17:05:58', '2019-09-23 17:05:58'),
(6, 'اشرف', 'داود', 'show@app.com', 'rGEQOmGj58DcuGnm5T87FTYrf4TCEPwznawskDNg.jpeg', NULL, '$2y$10$BDCQ3hrSEs.v5i5UU.7PVucX5HXLcBqkMbpkTRii7tiAjrNMDhO4e', 'u8j3BKT2GtIoCpqhG2SuSHIOccnCgobg1IoOdK1TAAlTGBYCmssynaYc5VhM', '2019-09-23 18:03:51', '2019-09-23 18:04:02');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category_translations`
--
ALTER TABLE `category_translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `category_translations_category_id_locale_unique` (`category_id`,`locale`),
  ADD KEY `category_translations_locale_index` (`locale`);

--
-- Indexes for table `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `orders_client_id_foreign` (`client_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permissions_name_unique` (`name`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_role_id_foreign` (`role_id`);

--
-- Indexes for table `permission_user`
--
ALTER TABLE `permission_user`
  ADD PRIMARY KEY (`user_id`,`permission_id`,`user_type`),
  ADD KEY `permission_user_permission_id_foreign` (`permission_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `products_category_id_foreign` (`category_id`);

--
-- Indexes for table `product_order`
--
ALTER TABLE `product_order`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_order_product_id_foreign` (`product_id`),
  ADD KEY `product_order_order_id_foreign` (`order_id`);

--
-- Indexes for table `product_translations`
--
ALTER TABLE `product_translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `product_translations_product_id_locale_unique` (`product_id`,`locale`),
  ADD KEY `product_translations_locale_index` (`locale`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indexes for table `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`user_id`,`role_id`,`user_type`),
  ADD KEY `role_user_role_id_foreign` (`role_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `category_translations`
--
ALTER TABLE `category_translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `clients`
--
ALTER TABLE `clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `product_order`
--
ALTER TABLE `product_order`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=337;

--
-- AUTO_INCREMENT for table `product_translations`
--
ALTER TABLE `product_translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `category_translations`
--
ALTER TABLE `category_translations`
  ADD CONSTRAINT `category_translations_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_client_id_foreign` FOREIGN KEY (`client_id`) REFERENCES `clients` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `permission_user`
--
ALTER TABLE `permission_user`
  ADD CONSTRAINT `permission_user_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `product_order`
--
ALTER TABLE `product_order`
  ADD CONSTRAINT `product_order_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `product_order_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `product_translations`
--
ALTER TABLE `product_translations`
  ADD CONSTRAINT `product_translations_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `role_user`
--
ALTER TABLE `role_user`
  ADD CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
