<?php

namespace App;

use Carbon\Carbon;
use Jenssegers\Date\Date;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Request;

class Order extends Model
{
    protected $guarded = [];
    protected $appends = ['date', 'ar_data', 'ar_month', 'monthorde'];

    public function client()
    {
        return $this->belongsTo(Client::class);
    } //end of 

    public function getDateAttribute()
    {
        return $this->created_at->toDateString();
    } //end of date


    public function getMonthordeAttribute()
    {
            return $this->created_at->month;
        
    } //end of date

    public function getArDataAttribute()
    {
        \Date::setLocale('ar');
        $date = Date::parse($this->created_at)->format('j F Y');

        return $date;
    } //end of date
    public function getArMonthAttribute()
    {
        \Date::setLocale('ar');
        // $date = Date::now()->format('l j F Y H:i:s');
        $date = Date::parse($this->created_at)->format(' F ');

        return $date;
    } //end of date

    public function products()
    {
        return $this->belongsToMany(Product::class, 'product_order')->withPivot('quantity');
    } //end of products

}//end of model
