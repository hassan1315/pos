<?php

namespace App\Http\Controllers\Dashboard;

use App\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class YearlyReportController extends Controller
{
    public function index()
    {
        return view('dashboard.yearly_report.index');
    } //end of index

    public function report(Request $request)
    {
        $dateStart = $request->year . '-' . $request->begain_month . '-' . '01';
        $dateEnd = $request->year . '-' . $request->last_month . '-' . '31';
        $grouped = Order::with('products')->whereBetween('created_at', [$dateStart . " 00:00:00", $dateEnd . " 23:59:59"])->get();
        $orders = $grouped->groupBy('monthorde');
        return view('dashboard.yearly_report._report', compact('orders'));
    } //end of report 
}//end of controller
