<?php

namespace App\Http\Controllers\Dashboard\Client;

use App\Category;
use App\Client;
use App\Order;
use App\Product;
use function foo\func;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OrderController extends Controller
{
    public function create(Client $client, Request $request)
    {
        $categories = Category::with('products')->get();
        $orders = $client->orders()->with('products')->paginate(10);
        return view('dashboard.clients.orders.create', compact('client', 'categories', 'orders'));
    } //end of create

    public function filter_product(Request $request)
    {

        if (request()->ajax()) {

            if (!empty($request->search)) {
                if (is_numeric($request->search)) {
                    $products = Product::when($request->search, function ($q) use ($request) {
                        return $q->where('id', $request->search);
                    })->when($request->category, function ($q) use ($request) {
                        return $q->where('category_id', $request->category);
                    })->latest()->paginate(10);
                } else {
                    $products = Product::when($request->search, function ($q) use ($request) {
                        return $q->whereTranslationLike('name', '%' . $request->search . '%');
                    })->when($request->category, function ($q) use ($request) {
                        return $q->where('category_id', $request->category);
                    })->latest()->paginate(10);
                }
                return view('dashboard.clients.orders._products', compact('products'));
            } else {
                return view('dashboard.clients.orders._products', compact('products'));
            }
        }
    }

    public function store(Request $request, Client $client)
    {
        $request->validate([
            'products' => 'required|array',
        ]);

        $this->attach_order($request, $client);

        session()->flash('success', __('site.added_successfully'));
        return redirect()->route('dashboard.orders.index');
    } //end of store

    public function edit(Client $client, Order $order)
    {
        $categories = Category::with('products')->get();
        $orders = $client->orders()->with('products')->paginate(5);
        return view('dashboard.clients.orders.edit', compact('client', 'order', 'categories', 'orders'));
    } //end of edit

    public function update(Request $request, Client $client, Order $order)
    {
        $request->validate([
            'products' => 'required|array',
        ]);

        $this->detach_order($order);

        $this->attach_order($request, $client);

        session()->flash('success', __('site.updated_successfully'));
        return redirect()->route('dashboard.orders.index');
    } //end of update

    private function attach_order($request, $client)
    {
        $total_price = str_replace(',', '', $request->total_price);
        $order = $client->orders()->create([
            'total_price' => $total_price
        ]);


        $order->products()->attach($request->products);


        foreach ($request->products as $id => $quantity) {

            $product = Product::FindOrFail($id);

            $product->update([
                'stock' => $product->stock - $quantity['quantity']
            ]);
        } //end of foreach
    } //end of attach order

    private function detach_order($order)
    {
        foreach ($order->products as $product) {

            $product->update([
                'stock' => $product->stock + $product->pivot->quantity
            ]);
        } //end of for each

        $order->delete();
    } //end of detach order

}//end of controller
