<?php

namespace App\Http\Controllers\Dashboard;

use App\Order;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MonthlyReportController extends Controller
{
    public function index()
    {
        return view('dashboard.monthly_report.index');
    } //end of index

    public function report(Request $request)
    {
        $dateStart = $request->year . '-' . $request->month . '-' . $request->begain_day;
        $dateEnd = $request->year . '-' . $request->month . '-' . $request->last_day;
        $grouped = Order::with('products')->whereBetween('created_at', [$dateStart . " 00:00:00", $dateEnd . " 23:59:59"])->get();
        $orders = $grouped->groupBy('date');
        return view('dashboard.monthly_report._report', compact('orders'));
    } //end of report 
}//end of controller
