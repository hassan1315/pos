<?php

namespace App\Http\Controllers\Dashboard;

use App\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class OrderController extends Controller
{
    public function index(Request $request)
    {
        $orders = Order::whereDate('created_at', '=', now()->toDateString())->whereHas('client', function ($q) use ($request) {
            return $q->where('name', 'like', '%' . $request->search . '%');
        })->with('products', 'client')->latest()->paginate(10);

        if (is_numeric($request->search)) {
            $orders = Order::when($request->search, function ($q) use ($request) {

                return $q->where('id', $request->search);
            })->with('products', 'client')->latest()->paginate(10);
        } else {
            if ($request->search) {
                $orders = Order::whereHas('client', function ($q) use ($request) {
                    return $q->where('name', 'like', '%' . $request->search . '%');
                })->with('products', 'client')->latest()->paginate(10);
            }
        }

        if ($request->year  and $request->month  and $request->day) {
            $date_need = $request->year . '-' . $request->month . '-' . $request->day;
            if ($request->year = 'all_years' and $request->month == 'all_months' and $request->day == 'all_days') {

                $orders =   Order::latest()->paginate(10);
                return view('dashboard.orders.index', compact('orders'));
            }
            $orders = Order::whereDate('created_at', '=', $date_need)->with('products', 'client')->latest()->paginate(10);
            return view('dashboard.orders.index', compact('orders'));
        }

        return view('dashboard.orders.index', compact('orders'));
    } //end of index


    public function products(Order $order)
    {
        $orders = Order::all();
        $products = $order->products;
        $order_id = $products->first()->pivot->order_id;
        $order = $orders->where('id', $order_id)->first();
        return view('dashboard.orders._products', compact('order', 'products'));
    } //end of products

    public function status(Order $order, Request $request)
    {
        if ($request->status == 0) {

            $order->update(['status' => 1]);
        }

        return view('dashboard.orders._status', compact('order'));
    } //end of products

    public function destroy(Order $order)
    {
        foreach ($order->products as $product) {

            $product->update([
                'stock' => $product->stock + $product->pivot->quantity
            ]);
        } //end of for each

        $order->delete();
        session()->flash('success', __('site.deleted_successfully'));
        return redirect()->route('dashboard.orders.index');
    } //end of order

}//end of controller
