<?php

namespace App\Http\Controllers\Dashboard;

use App\Order;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RevenuesController extends Controller
{
    public function index(Request $request)
    {
        return view('dashboard.revenues.index');
    } //end of index

    public function report(Request $request)
    {
        $date_need = $request->year . '-' . $request->month . '-' . $request->day;
        $orders = Order::whereDate('created_at', '=', $date_need)->with('products', 'client')->get();
        return view('dashboard.revenues._report', compact('orders'));
    } //end of report

}//end of controller
