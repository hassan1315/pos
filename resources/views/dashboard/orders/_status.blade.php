
<a                                                                                 
    class="order-status-btn {{ $order->status == 0 ? ' btn btn-warning btn-sm' : 'label label-success ' }} "
>
@if ($order->status == 0)              
    @lang('site.processing')
@else
    @lang('site.finished')
@endif
</a>       
                                                                                                