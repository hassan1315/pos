
<div id="print-area">
    <h3 style="text-align:center; margin: -10px 0 25px">@lang('site.sales_bill')</h3>

    
    <div class="row" >
        <div class="col-md-7">
            <p>@lang('site.client_name') : <span>{{ $order->client->name }}</span></p>
        </div>
        
        <div class="col-md-5">
            <p>@lang('site.bill_number') :  <span>{{$products->first()->pivot->order_id}}</span></p>
        </div>
    </div>
    <div class="row" >
        <div class="col-md-7">
            <p>@lang('site.date') :  <span>{{ $order->created_at->toDateString('Y-M-D') }}</span></p>
        </div>
        <div class="col-md-5">
            <p>@lang('site.hour') :  <span>{{  $order->created_at->toTimeString("H:i:s") }}</span></p>
        </div>
     
        


    </div>
    <table class="table table-hover table-bordered" style="margin-top:-15px !important">
        <thead>
            <tr>
                <th>@lang('site.product_name')</th>
                <th>@lang('site.price')</th>
                <th>@lang('site.quantity')</th>
                <th>@lang('site.total_price')</th>
            </tr>
        </thead>

        <tbody>
        @foreach ($products as $product)
            <tr>
                <td>{{ $product->name }}</td>
                <td>{{ number_format($product->sale_price)}} @lang('site.pound')</td>
                <td>{{ $product->pivot->quantity }}</td>
                <td>{{ number_format($product->pivot->quantity * $product->sale_price) }} @lang('site.pound')</td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <h3>@lang('site.total') <span>{{ number_format($order->total_price) }} @lang('site.pound')</span></h3>

</div>

<button class="btn btn-block btn-primary print-btn"><i class="fa fa-print"></i> @lang('site.print')</button>
