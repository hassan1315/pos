@extends('layouts.dashboard.app')

@section('content')

    <div class="content-wrapper">

        <section class="content-header">

            <h1>@lang('site.orders')
                <small>{{ $orders->total() }} @lang('site.orders')</small>
            </h1>

            <ol class="breadcrumb">
                <li><a href="{{ route('dashboard.welcome') }}"><i class="fa fa-dashboard"></i> @lang('site.dashboard')</a></li>
                <li class="active">@lang('site.orders')</li>
            </ol>
        </section>

        <section class="content">
            <div class="row">
                <div class="col-md-7">
                    <div class="box box-primary">
                        <div class="box-header">
                            <h3 class="box-title" style="margin-bottom: 10px">@lang('site.orders')</h3>
                            <div class="row">
                                <div class="col-md-4">
                                    <form action="{{ route('dashboard.orders.index') }}" method="get">
                                        <div class="row">
                                            <div class="col-md-9">
                                                @php
                                                    $order_id = request()->order_id;
                                                    $search = request()->search
                                                @endphp
                                            <input type="text" name="search" class="form-control" placeholder="@lang('site.search')" {{ !empty(request()->order_id) ? 'value='.$order_id.'' : 'value='.$search.''}}>
                                            </div>
                                            <div class="col-md-2">
                                                <button type="submit"  
                                                    @if (config('app.locale') == 'ar') style="margin-right:-25px;" @endif 
                                                    class="btn btn-primary">
                                                    <i class="fa fa-search"></i>
                                                    @lang('site.search')
                                                </button> 
                                            </div>
                                        </div><!-- end of row -->
                                    </form><!-- end of form -->
                                </div>
                                <div class="col-md-7">
                                    <form action="{{ route('dashboard.orders.index') }}" method="get">
                                        
                                        <div class="row">
                                            <div class="col-md-4">
                                                <select name="day" class="form-control">
                                                    <option value="{{now()->day}}">@lang('site.this_day')</option>
                                                    <option value="all_days">@lang('site.all_days')</option>
                                                    @for ( $i = 1 ; $i <= 30 ; $i++)                                                
                                                        <option   value="{{$i}}">{{$i}}</option>
                                                    @endfor
                                                </select>
                                            </div>

                                            <div class="col-md-4" style="margin-right:-20px;" >
                                                <select name="month" class="form-control">
                                                    <option value="{{now()->month}}">@lang('site.this_month')</option>
                                                    <option value="all_months">@lang('site.all_month')</option>
                                                    @for ( $i = 1 ; $i <= 12 ; $i++)                                                
                                                        <option   value="{{$i}}">{{$i}}</option>
                                                    @endfor
                                                </select>
                                            </div>

                                            <div class="col-md-4" style="margin-right:-20px;">
                                                <select name="year" class="form-control">
                                                    <option value="{{now()->year}}">@lang('site.this_year')</option>
                                                    <option value="all_years">@lang('site.all_years')</option>
                                                    @for ( $i = 2019 ; $i <= 2019 ; $i++)                                                
                                                        <option   value="{{$i}}">{{$i}}</option>
                                                    @endfor
                                                </select>
                                            </div>
                                            <div class="col-md-1" style="margin-right: -25px">
                                                <button type="submit"  
                                                    class="btn btn-info">
                                                    <i class="fa fa-info-circle"></i> 
                                                    @lang('site.show')
                                                </button> 
                                            </div>
                                        </div><!-- end of row -->
                                    </form><!-- end of form -->
                                </div>
                            </div>

                        </div><!-- end of box header -->

                        @if ($orders->count() > 0)

                            <div class="box-body table-responsive">

                                <table class="table table-hover">
                                    <tr>
                                        <th>#</th>
                                        <th>@lang('site.client_name')</th>
                                        <th>@lang('site.price')</th>
                                        <th>@lang('site.status')</th>
                                        <th>@lang('site.created_at')</th>
                                        <th>@lang('site.action')</th>
                                    </tr>

                                    @foreach ($orders as $index=>$order)
                                        <tr>
                                            <td>{{ $index + 1  }}</td>
                                            <td>{{ $order->client->name }}</td>       
                                             <td>{{ number_format($order->total_price) }} @lang('site.pound')</td>
                                            <td id="status_data{{$order->id}}">
                                                <a                                                                                 
                                                    data-url="{{ route('dashboard.orders.update_status', $order->id) }}"
                                                    data-id="{{$order->id }}"
                                                    data-method="get"
                                                    data-status=" @if ($order->status == 0)              
                                                    @lang('site.processing')
                                                @else
                                                    @lang('site.finished')
                                                @endif"
                                                    class="order-status-btn  {{ $order->status == 0 ? 'btn btn-warning btn-sm' : 'label label-success ' }} "
                                                >
                                                @if ($order->status == 0)              
                                                    @lang('site.processing')
                                                @else
                                                    @lang('site.finished')
                                                @endif
                                                </a>                                                                                             
                                            </td>
                                            <td>{{ $order->created_at->toDateString() }}</td>
                                            <td>
                                                <button class="btn btn-primary btn-sm order-products"
                                                        data-url="{{ route('dashboard.orders.products', $order->id) }}"
                                                        data-method="get"
                                                >
                                                    <i class="fa fa-list"></i>
                                                    @lang('site.show')
                                                </button>
                                                @if (auth()->user()->hasPermission('update_orders'))
                                                    <a href="{{ route('dashboard.clients.orders.edit', ['client' => $order->client->id, 'order' => $order->id]) }}" class="btn btn-info btn-sm"><i class="fa fa-pencil"></i> @lang('site.edit')</a>
                                                
                                                @endif

                                                @if (auth()->user()->hasPermission('delete_orders'))
                                                    <form action="{{ route('dashboard.orders.destroy', $order->id) }}" method="post" style="display: inline-block;">
                                                        {{ csrf_field() }}
                                                        {{ method_field('delete') }}
                                                        <button type="submit" class="btn btn-danger btn-sm delete"><i class="fa fa-trash"></i> @lang('site.delete')</button>
                                                    </form>

                                                
                                                @endif

                                            </td>

                                        </tr>

                                    @endforeach

                                </table><!-- end of table -->

                                {{ $orders->appends(request()->query())->links() }}

                            </div>

                        @else

                            <div class="box-body">
                                <h3>@lang('site.no_records')</h3>
                            </div>

                        @endif

                    </div><!-- end of box -->

                </div><!-- end of col -->

                <div class="col-md-5"   >

                    <div class="box box-primary">

                        <div class="box-header">
                            <div style="display: none; flex-direction: column; align-items: center; margin-top:10px;" id="loading">
                                <div class="loader"></div>
                                <h5 style="margin-top: 10px">@lang('site.loading')</h5>
                            </div>
                        </div><!-- end of box header -->

                        <div id="order-product-list" class=" box-body">
                            <h3 style="text-align: center; margin-bottom: 35px; line-height: 1.5">@lang('site.choice_order_to_show_bill') </h3 style="text-align: center">
                            <!-- Order Product Loaded By Ajax Here-->
                        </div><!-- end of box body -->

                    </div><!-- end of box -->
                </div><!-- end of col -->
            </div><!-- end of row -->
        </section><!-- end of content section -->
    </div><!-- end of content wrapper -->

@endsection
