@extends('layouts.dashboard.app')

@section('content')

    <div class="content-wrapper">

        <section class="content-header">

            <h1>@lang('site.revenues')
            </h1>

            <ol class="breadcrumb">
                <li><a href="{{ route('dashboard.welcome') }}"><i class="fa fa-dashboard"></i> @lang('site.dashboard')</a></li>
                <li class="active">@lang('site.revenues')</li>
            </ol>
        </section>

        <section class="content">
            <div class="row">
                <div class="col-md-4">
                    <div class="box box-primary">
                        <div class="box-header">
                            <h3 class="box-title" style="margin-bottom: 10px">@lang('site.revenues')</h3>
                        </div><!-- end of box header -->
                        <div class="box-body table-responsive">          
                            <form action="{{ route('dashboard.revenues.report') }}" method="get">
                                <div style="margin: 10px 0px">
                                    <label>@lang('site.day')</label>
                                    <select name="day" class="day form-control">
                                        <option value="{{now()->day}}">@lang('site.this_day')</option>
                                        @for ( $i = 1 ; $i <= 31 ; $i++)                                                
                                            <option value="{{$i}}">{{$i}}</option>
                                        @endfor
                                    </select>
                                </div>
                                
                                <div style="margin: 10px 0px">
                                            <label>@lang('site.month')</label>
                                    <select name="month" class="month form-control">
                                        <option value="{{now()->month}}">@lang('site.this_month')</option>
                                        @for ( $i = 1 ; $i <= 12 ; $i++)                                                
                                        <option value="{{$i}}">{{$i}}</option>
                                        @endfor
                                    </select>
                                </div>
                                
                                <div style="margin: 10px 0px">
                                    <label>@lang('site.year')</label>
                                    <select name="year" class="year form-control">
                                    <option value="{{now()->year}}">@lang('site.this_year')</option>
                                    @php
                                        $now_year = now()->year
                                    @endphp
                                        @for ( $i = 2019 ; $i <= $now_year ; $i++)                                                
                                            <option value="{{$i}}">{{$i}}</option>
                                        @endfor
                                    </select>
                                </div>
                                  
                               <button 
                                    data-url = {{ route('dashboard.revenues.report') }}
                                    data-method = "get"
                                    style="margin: 15px 0" 
                                    class="btn btn-block btn-info btn-report">
                                    <i class="fa fa-info-circle"></i> @lang('site.show_report')
                               </button>
                            </form><!-- end of form -->
                        </div>
                    </div><!-- end of box -->
                </div><!-- end of col -->

                <div class="col-md-8">
                    <div class="box box-primary">

            
                        <div class="box-header">
                            <div style="display: none; flex-direction: column; align-items: center;" id="loading">
                                <div class="loader"></div>
                                <p style="margin-top: 10px">@lang('site.loading')</p>
                            </div>

                            <div class="box-body">
                                <div id="report">
                                    <h2 style="text-align: center; margin-bottom: 20px;">@lang('site.choice_day_to_show_report')</h2>
                                </div>
                            </div><!-- end of order product list -->
                            
                        </div><!-- end of box body -->
                    </div><!-- end of box -->
                </div><!-- end of row -->
        </section><!-- end of content section -->

    </div><!-- end of cntent wrapper -->

@endsection