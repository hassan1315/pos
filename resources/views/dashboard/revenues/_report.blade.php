@if ($orders->count() > 0)
    
<div id="print-area">
    <h2 style="text-align:center; margin:-5px 0 25px">التقرير اليومى للمبيعات </h2>
    <div class="row" style="margin: 5px 0 0 0 !important">

        <div class="col-md-8">
            <h5><b>@lang('site.date')</b>  :  <span>{{$orders->first()->ar_data}}</span></h5>
        </div>
        <div class="col-md-4">
            <h5><b>@lang('site.orders_count')</b> :  <span>{{$orders->count()}} @lang('site.order_small')</span></h5>
        </div>
    </div>
    <table class="table table-hover table-bordered">
        <thead>
            <tr>
                <th>#</th>
                <th>@lang('site.client_name')</th>
                <th>@lang('site.order_number')</th>
                <th>@lang('site.quantity')</th>
                <th>@lang('site.price')</th>
                <th>@lang('site.status')</th>
                <th>@lang('site.action')</th>
            </tr>
        </thead>

        <tbody>
           @foreach ($orders as $index=>$order)
               <tr>
                    @foreach ($order->products as $product)
                       @php
                       global $product_n;
                           $product_n +=  $product->pivot->quantity;
                       @endphp
                   @endforeach
                    <td>{{ $index + 1  }}</td>
                    <td>{{ $order->client->name }}</td>
                    <td>{{ $order->id }}</td>
                    <td>{{ $product_n }} @lang('site.product_small')
                    @php
                        $product_n = 0;
                    @endphp
                    </td>
                    <td>{{ number_format($order->total_price) }} @lang('site.pound')</td>
                    <td>
                        <span                                                                                 
                            @if ($order->status == 0)              
                            @lang('site.processing')
                        @else
                            @lang('site.finished')
                        @endif
                            class="order-status-btn  {{ $order->status == 0 ? 'btn btn-warning btn-sm' : 'label label-success ' }} "
                        >
                        @if ($order->status == 0)              
                            @lang('site.processing')
                        @else
                            @lang('site.finished')
                        @endif
                        </a>                                                                                             
                    </td>
                    <td><a href="{{ route('dashboard.orders.index',['search' => $order->id]) }}" class='btn btn-info btn-sm'> عرض</a></td>
                </tr>
            @endforeach
        
        </tbody>
    </table>
    <hr>
    @php
        global $profit_n;
        global $product_n;
        global $total_count;
        global $product_count;
        global $profit_count;
    @endphp
    @foreach ($orders as $order)
        @php
            $total_count +=  $order->total_price
        @endphp
        @foreach ($order->products as $product)
            @php
                $product_n +=  $product->pivot->quantity;
                $profit_n +=  $product->profit;
            @endphp
        @endforeach
            @php
                $product_count +=  $product_n;
                $product_n = 0;
                $profit_count +=  $profit_n;
                $profit_n = 0;
            @endphp
    @endforeach

    <h4><b>@lang('site.total_count')</b> : &nbsp; <span> {{ number_format($total_count) }} @php $total_count = 0;  @endphp</span><span>@lang('site.egyption_pound')</span></h4>
    <h4><b>@lang('site.profit_total')</b> : <span> {{ number_format($profit_count) }} @php $profit_count = 0;  @endphp</span><span>@lang('site.egyption_pound')</span></h4>
    <h4><b>@lang('site.product_count_sales')</b> : <span> {{ $product_count }} @php $product_count = 0; $product_n = 0; @endphp</span><span>@lang('site.product_small')</span></h4>
    <br>
</div>

<button class="btn btn-block btn-primary print-btn"><i class="fa fa-print"></i> @lang('site.print')</button>
@else
 <h1 style="text-align: center">@lang('site.no_records')</h1>
 @endif