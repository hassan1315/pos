@if ($orders->count() > 0)
<div id="print-area">
    <h2 style="text-align:center; margin:5px 0 30px">@lang('site.monthly_report_for_sales')</h2>
    <div class="row" style="margin: 5px 0 0 0 !important">
        <div class="col-md-8">
            <h5><b>@lang('site.from_date') : &nbsp </b><span> {{$orders->first()->first()->ar_data}} </span></h5>
        </div>

        <div class="col-md-4">
            <h5><b>@lang('site.to_date') : &nbsp</b><span>{{$orders->last()->first()->ar_data}}</span></h5>
        </div>
    </div>

    <table class="table table-hover table-bordered">
        <thead>
            <tr>
                <th>@lang('site.count')</th>
                <th>@lang('site.date')</th>
                <th>@lang('site.order_count')</th>
                <th>@lang('site.product_count')</th>
                <th>@lang('site.total_count')</th>
                <th>@lang('site.profit_total')</th>
                <th>@lang('site.action')</th>
            </tr>
        </thead>
        <tbody>         
        @php
            global $order_n,
                   $order_count,
                   $total_n,
                   $total_count,
                   $quantity_n,
                   $quantity_count,
                   $profit_n,
                   $profit_count,
                   $i;
        @endphp
        @foreach ($orders as $index=>$order)
            @foreach ($order as $index=>$ord)
                @php
                    $order_n += $ord->status;
                    $total_n += $ord->total_price;
                @endphp
                @foreach ($ord->products as $product) 
                    @php
                        $quantity_n += $product->pivot->quantity;
                        $profit_n += $product->profit;
                    @endphp
                @endforeach
            @endforeach
            @php
                $order_count += $order_n;
                $total_count += $total_n;
                $quantity_count += $quantity_n;
                $profit_count += $profit_n;
            @endphp
          
            <tr>
                <td> @if ($i < 1) 0 @endif{{ $i++ }}</td>
                <td>{{ $order->first()->ar_data }}</td>
                <td>{{$order_n }}@php $order_n = 0 @endphp @lang('site.order_small')</td>
                <td>{{$quantity_n}}@php $quantity_n = 0 @endphp @lang('site.product_small')</td>
                <td>{{number_format($total_n)}} @php $total_n = 0 @endphp @lang('site.pound')</td>
                <td>{{number_format($profit_n)}} @php $profit_n = 0 @endphp @lang('site.pound')</td>
                <td><a href="{{route('dashboard.revenues.index',['date' => $order->first()->date])}}" class='btn btn-info btn-sm'> @lang('site.show')</a></td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <hr>
  
    <h4><b>@lang('site.order_count') : &nbsp;</b><span> {{ $order_count }} @php $order_count = 0; @endphp</span><span>@lang('site.order_small')</span></h4> 
    <h4><b>@lang('site.product_count') : &nbsp;</b><span> {{ $quantity_count }} @php $quantity_count = 0; @endphp</span><span>@lang('site.product_small')</span></h4> 
    <h4><b>@lang('site.total_count') : &nbsp;</b><span> {{ number_format($total_count) }} @php $total_count = 0;  @endphp</span><span>@lang('site.egyption_pound')</span></h4>
    <h4><b>@lang('site.profit_total') : &nbsp;</b><span> {{ number_format($profit_count) }} @php $profit_count = 0;  @endphp</span><span>@lang('site.egyption_pound')</span></h4>
    <br>
</div>

<button class="btn btn-block btn-primary print-btn"><i class="fa fa-print"></i> @lang('site.print')</button>
@else
 <h1 style="text-align: center">@lang('site.no_records')</h1>
@endif