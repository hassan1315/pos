<aside class="main-sidebar">

    <section class="sidebar">
<div class="user-panel">
            <div class="pull-left image">
                <img src="{{ Auth::user()->image_path }}" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
            <p>{{ Auth::user()->first_name}} {{ Auth::user()->last_name}}</p>
                <a href="#"><i class="fa fa-circle text-success"></i>@lang('site.active_now')</a>
            </div>
        </div><!-- search form -->
            <form action="#" method="get" class="sidebar-form">
                <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="@lang('site.search')">
                <span class="input-group-btn">
                        <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                        </button>
                    </span>
                </div>
        </form><!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">@lang('site.main_navgation')</li>
            
            <li><a href="{{ route('dashboard.welcome') }}"><i class="fa fa-dashboard"></i><span>@lang('site.dashboard')</span></a></li>
            
    
            @if (auth()->user()->hasPermission('read_categories'))
                <li><a href="{{ route('dashboard.categories.index') }}"><i class="fa fa-book"></i><span>@lang('site.categories')</span></a></li>
            @endif

            @if (auth()->user()->hasPermission('read_products'))
                <li><a href="{{ route('dashboard.products.index') }}"><i class="fa  fa-cubes"></i><span>@lang('site.products')</span></a></li>
            @endif

            @if (auth()->user()->hasPermission('read_clients'))
                <li><a href="{{ route('dashboard.clients.index') }}"><i class="fa  fa-male"></i><span>@lang('site.clients')</span></a></li>
            @endif

            @if (auth()->user()->hasPermission('read_orders'))
                <li><a href="{{ route('dashboard.orders.index') }}"><i class="fa fa-briefcase"></i><span>@lang('site.orders')</span></a></li>
            @endif
            
            @if (auth()->user()->whereRole('super_admin'))
            <li><a href="{{ route('dashboard.revenues.index') }}"><i class="fa fa-money"></i><span>@lang('site.revenues')</span></a></li>
            @endif

            @if (auth()->user()->whereRole('super_admin'))
               <li><a href="{{ route('dashboard.monthlyReport.index') }}"><i class="fa  fa-info-circle"></i><span>التقرير الشهرى</span></a></li>
           @endif
            
            @if (auth()->user()->whereRole('super_admin'))
                <li><a href="{{ route('dashboard.yearlyReport.index') }}"><i class="fa  fa-info-circle"></i><span>التقرير السنوى</span></a></li>
            @endif
            

            @if (auth()->user()->hasPermission('read_users'))
                <li><a href="{{ route('dashboard.users.index') }}"><i class="fa  fa-users"></i><span>@lang('site.users')</span></a></li>
            @endif
            
            
         
        </ul>

    </section>

</aside>

