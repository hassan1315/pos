<?php

use Illuminate\Database\Seeder;

class ClientsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        \App\Client::create([
            'name' => 'حسن محمد',
            'phone' => '01005164154',
            'address' => 'قليوب البلد حى سيدى عبد الرحمن ',
        ]);
        \App\Client::create([
            'name' => 'ايهاب السيسى',
            'phone' => '01078965896',
            'address' => 'الخانكة طريق شبين القناطر/ سندوه',
        ]);
        \App\Client::create([
            'name' => 'حميد بلابل',
            'phone' => '01064564868',
            'address' => 'المنوفيه / قويسنا طه شبرا',
        ]);
        \App\Client::create([
            'name' => 'اشرف داود',
            'phone' => '01138464882',
            'address' => 'القناطر الخيريه طريق بلتان',
        ]);
        \App\Client::create([
            'name' => 'احمد البرادعى',
            'phone' => '01568464532',
            'address' => 'الغربية طنطا كفر مسعود',
        ]);
    } //end of run

}//end of seeder
