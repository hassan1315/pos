<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /* Tv Catrogry */
        \App\Product::create([
            'category_id' =>  1,
            'ar' => ['name' => 'جهاز توشييا 32 بوصه', 'description' => 'هذا وصف جهاز توشييا 32 بوصه'],
            'en' => ['name' => 'Toshiba Tv 32 inches', 'description' => 'this is description for '],
            'purchase_price' => 1500,
            'sale_price' => 2000,
            'stock' => 80,
        ]);

        \App\Product::create([
            'category_id' =>  1,
            'ar' => ['name' => 'تلفزيون سامسونج 48 بوصة', 'description' => ' هذا وصف   '],
            'en' => ['name' => 'Tv samsong 32 inches', 'description' => 'this is description for Tv samsong 32 inches'],
            'purchase_price' => 2000,
            'sale_price' => 2300,
            'stock' => 100,
        ]);

        \App\Product::create([
            'category_id' =>  1,
            'ar' => ['name' => 'سماعة بلوتوث اير بود', 'description' => 'هذا وصف سماعة بلوتوث اير بود'],
            'en' => ['name' => 'Air Pods High Quality', 'description' => 'this is description for air pods high quality'],
            'purchase_price' => 1500,
            'sale_price' => 2200,
            'stock' => 100,
        ]);

        \App\Product::create([
            'category_id' =>  1,
            'ar' => ['name' => 'باور بانك 1000 ملى امبير', 'description' => 'هذا وصف باور بانك 1000 ملى امبير'],
            'en' => ['name' => 'Power Pank 10000 MA', 'description' => 'this is description for Power Pank 10000 MA'],
            'purchase_price' => 380,
            'sale_price' => 450,
            'stock' => 100,
        ]);

        \App\Product::create([
            'category_id' =>  1,
            'ar' => ['name' => 'سماعات عالية المستوى', 'description' => 'هذ وثف سماعات عالية المستوى'],
            'en' => ['name' => 'Sound Dj HIgh Sound', 'description' => 'this is description for Sound Dj HIgh Sound '],
            'purchase_price' => 600,
            'sale_price' => 700,
            'stock' => 100,
        ]);

        /***********************************************************/

        /* Computer And Lab Category */
        \App\Product::create([
            'category_id' =>  2,
            'ar' => ['name' => 'لاب توب hp core i8 ram 8Gb', 'description' => 'لاب توب hp core i8 هذا وصف'],
            'en' => ['name' => 'laptop core i8 8gb ram', 'description' => 'this is description for laptop core i8 8gb ram'],
            'purchase_price' => 12000,
            'sale_price' => 13000,
            'stock' => 15,
        ]);

        \App\Product::create([
            'category_id' =>  2,
            'ar' => ['name' => 'شاشة كمبيوتر 32 بوصه ', 'description' => 'شاشة كمبيوتر 32 بوصه '],
            'en' => ['name' => 'Lcd Computer 32 inches', 'description' => 'this is description for Lcd Computer 32 inches'],
            'purchase_price' => 1500,
            'sale_price' => 2000,
            'stock' => 40,
        ]);

        \App\Product::create([
            'category_id' =>  2,
            'ar' => ['name' => 'سماعات كمبيوتر عالية الدقة', 'description' => 'هذا وصف سماعات كمبيوتر عالية الدقة'],
            'en' => ['name' => 'Speaker HD High Quality', 'description' => 'this is description for Speaker HD High Quality'],
            'purchase_price' => 650,
            'sale_price' => 700,
            'stock' => 100,
        ]);

        \App\Product::create([
            'category_id' =>  2,
            'ar' => ['name' => 'كيبورد جيمينج', 'description' => ' هذا وصف كيبورد جيمينج'],
            'en' => ['name' => 'Keyboard Gaming', 'description' => 'this is description for Keyboard Gaming'],
            'purchase_price' => 120,
            'sale_price' => 180,
            'stock' => 40,
        ]);

        \App\Product::create([
            'category_id' =>  2,
            'ar' => ['name' => 'ماوس جيمينج', 'description' => ' هذا وصف ماوس جيمينج'],
            'en' => ['name' => 'Mouse Gaming', 'description' => 'this is description for Mouse Gaming'],
            'purchase_price' => 120,
            'sale_price' => 180,
            'stock' => 40,
        ]);

        \App\Product::create([
            'category_id' =>  2,
            'ar' => ['name' => 'رامات كمبيوتر 8GB', 'description' => ' هذا وصف رامات كمبيوتر 8GB '],
            'en' => ['name' => 'Ram 8GB Gaming', 'description' => 'this is description for Ram 8GB Gaming'],
            'purchase_price' => 500,
            'sale_price' => 700,
            'stock' => 40,
        ]);

        /***********************************************************/

        /*Clothes Wash Category*/
        \App\Product::create([
            'category_id' =>  3,
            'ar' => ['name' => 'غسالة اوتماتيك توشيبا', 'description' => 'هذا وصف غسالة اوتماتيك توشيبا'],
            'en' => ['name' => 'Clothes Wash Autmatic', 'description' => 'this is description for Clothes Wash Autmatic'],
            'purchase_price' => 3000,
            'sale_price' => 3200,
            'stock' => 20,
        ]);

        \App\Product::create([
            'category_id' =>  3,
            'ar' => ['name' => 'تلاجة توشيبا 21 قدم', 'description' => ' هذا وصف تلاجة توشيبا 21 قدم'],
            'en' => ['name' => 'Frizze Toshiba 21 ', 'description' => 'this is description for Frizze Toshiba 21'],
            'purchase_price' => 2300,
            'sale_price' => 3500,
            'stock' => 40,
        ]);

        \App\Product::create([
            'category_id' =>  3,
            'ar' => ['name' => 'مروحة فريش اوتامتيك بريموت', 'description' => 'هذا وصف مروجة فريش اوتامتيك بريموت'],
            'en' => ['name' => 'Fan Fresh High Quality', 'description' => 'this is description for Fan Fresh High Quality'],
            'purchase_price' => 800,
            'sale_price' => 850,
            'stock' => 30,
        ]);

        \App\Product::create([
            'category_id' =>  3,
            'ar' => ['name' => 'غسالة ابطاق 25 بطق', 'description' => 'هذا وصف غسالة ابطاق 25 بطق'],
            'en' => ['name' => 'dish Wash 23 dish', 'description' => 'this is description for dish Wash 23 dish'],
            'purchase_price' => 1200,
            'sale_price' => 1300,
            'stock' => 30,
        ]);

        \App\Product::create([
            'category_id' =>  3,
            'ar' => ['name' => 'مايكروف باشعه ليزر', 'description' => 'هذا وصف مايكروف باشعه ليزر'],
            'en' => ['name' => 'Microwave With Liser', 'description' => 'this is description for Microwave With Liser'],
            'purchase_price' => 500,
            'sale_price' => 600,
            'stock' => 30,
        ]);

        /*********************************************************** */

        /* Clothes and makeup Category */
        \App\Product::create([
            'category_id' =>  4,
            'ar' => ['name' => 'تي شيرت بولو سادة بطبعة لوجو ', 'description' => 'هذ وصف تي شيرت بولو سادة بطبعة لوجو '],
            'en' => ['name' => 'T-shirt Solid with logo', 'description' => 'this is description for T-shirt Solid with logo'],
            'purchase_price' => 180,
            'sale_price' => 230,
            'stock' => 100,
        ]);

        \App\Product::create([
            'category_id' =>  4,
            'ar' => ['name' => 'سويت شيرت هودي بدون أكمام', 'description' => 'هذ وصف سويت شيرت هودي بدون أكمام'],
            'en' => ['name' => 'Swet T-shirt black ', 'description' => 'this is description for Swet T-shirt'],
            'purchase_price' => 200,
            'sale_price' => 230,
            'stock' => 100,
        ]);

        \App\Product::create([
            'category_id' =>  4,
            'ar' => ['name' => 'بنطلون بيجاما بنمط مربعات', 'description' => 'هذ وصف بنطلون بيجاما بنمط مربعات'],
            'en' => ['name' => 'pantloan pjama blue', 'description' => 'this is description for pantloan pjama blue'],
            'purchase_price' => 170,
            'sale_price' => 200,
            'stock' => 100,
        ]);

        \App\Product::create([
            'category_id' =>  4,
            'ar' => ['name' => 'جاكيت بومبر مزين بطبع', 'description' => 'هذا وصف جاكيت بومبر مزين بطبع'],
            'en' => ['name' => 'Jaket Bump With Logo', 'description' => 'this is description for Jaket Bump With Logo'],
            'purchase_price' => 300,
            'sale_price' => 350,
            'stock' => 60,
        ]);

        \App\Product::create([
            'category_id' =>  4,
            'ar' => ['name' => 'بنطال جينز سادة', 'description' => 'هذ وصف بنطال جينز سادة'],
            'en' => ['name' => 'Pantlon Jeans Sloid', 'description' => 'this is description for Pantlon Jeans Sloid'],
            'purchase_price' => 170,
            'sale_price' => 200,
            'stock' => 100,
        ]);
    } //end of run

}//end of seeder
