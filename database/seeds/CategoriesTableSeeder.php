<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        \App\Category::create([
            'ar' => ['name' => 'قسم الالكترونيات'],
            'en' => ['name' => 'Tv Category'],
        ]);
        \App\Category::create([
            'ar' => ['name' => 'قسم الكمبيوتر '],
            'en' => ['name' => 'Computer Category'],
        ]);
        \App\Category::create([
            'ar' => ['name' => 'قسم الاجهزه الكهربية  '],
            'en' => ['name' => 'Clothes Wash Category'],
        ]);
        \App\Category::create([
            'ar' => ['name' => 'قسم الملابس '],
            'en' => ['name' => 'Clothes Category'],
        ]);
    } //end of run

}//end of seeder
