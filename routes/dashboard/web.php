<?php

Route::group(
    ['prefix' => LaravelLocalization::setLocale(), 'middleware' => ['localeSessionRedirect', 'localizationRedirect', 'localeViewPath']],
    function () {

        Route::prefix('dashboard')->name('dashboard.')->middleware(['auth'])->group(function () {

            Route::get('/', 'WelcomeController@index')->name('welcome');

            //category routes
            Route::resource('categories', 'CategoryController')->except(['show']);

            //product routes
            Route::resource('products', 'ProductController')->except(['show']);

            //client routes
            Route::resource('clients', 'ClientController')->except(['show']);
            Route::resource('clients.orders', 'Client\OrderController')->except(['show']);
            Route::get('clients/orders/{category}/product', 'Client\OrderController@filter_product')->name('clients.orders.product');

            //order routes
            Route::resource('orders', 'OrderController')->except(['show']);
            Route::get('/orders/{order}/products', 'OrderController@products')->name('orders.products');
            Route::get('/orders/{order}/status', 'OrderController@status')->name('orders.update_status');
            Route::get('/orders/filter', 'OrderController@filter')->name('orders.filter');

            //user revenues
            Route::resource('revenues', 'RevenuesController')->except(['show']);
            Route::get('/revenues/report', 'RevenuesController@report')->name('revenues.report');

            //user monthlyReport
            Route::resource('monthlyReport', 'MonthlyReportController')->except(['show']);
            Route::get('monthlyReport/report', 'MonthlyReportController@report')->name('monthlyReport.report');

            //user yearlyReport
            Route::resource('yearlyReport', 'YearlyReportController')->except(['show']);
            Route::get('yearlyReport/report', 'YearlyReportController@report')->name('yearlyReport.report');

            //user routes
            Route::resource('users', 'UserController')->except(['show']);
        }); //end of dashboard routes
    }
);
